<?php

return [
    'title' => 'Query',
    'description' => 'Generate Query/Expression',
    'arguments' => 'name [category]',
    'min_args' => 1,
];
