<?php

return [
    'description' => 'Generate Concern',
    'arguments' => 'name',
    'min_args' => 1,
];
