<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Expressions\MatchAll;

class MatchAllTest extends TestCase
{
    public function setUp()
    {
        $this->query = new MatchAll;
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query->key());
        $this->assertEquals('match_all', $this->query->key());
    }
}
