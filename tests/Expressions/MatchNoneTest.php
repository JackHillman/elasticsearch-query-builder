<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Expressions\MatchNone;

class MatchNoneTest extends TestCase
{
    public function setUp()
    {
        $this->query = new MatchNone;
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query->key());
        $this->assertEquals('match_none', $this->query->key());
    }
}
