<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Expressions\FullText\Match;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Resolver;
use ElasticsearchQueryBuilder\Contracts\Concerns\{
    CutoffFrequencyConcern,
    FuzzinessConcern,
    MinimumShouldMatchConcern,
    OperatorConcern,
    SynonymsConcern,
    ZeroTermsQueryConcern
};

class MatchTest extends TestCase
{
    protected function query()
    {
        return new Match;
    }

    public function setUp()
    {
        $this->query = new Match;
        $this->builder = new Builder(new Resolver);
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query->key());
        $this->assertEquals('match', $this->query->key());
    }

    public function testQueryImplementsRequiredContracts()
    {
        $this->assertInstanceOf(CutoffFrequencyConcern::class, $this->query);
        $this->assertInstanceOf(FuzzinessConcern::class, $this->query);
        $this->assertInstanceOf(MinimumShouldMatchConcern::class, $this->query);
        $this->assertInstanceOf(OperatorConcern::class, $this->query);
        $this->assertInstanceOf(SynonymsConcern::class, $this->query);
        $this->assertInstanceOf(ZeroTermsQueryConcern::class, $this->query);
    }

    public function testQueryBuildsBasicQuery()
    {
        $query = $this->query()->field('message')->query('this is a test');

        $this->assertEquals([
            'match' => [
                'message' => [
                    'query' => 'this is a test',
                ],
            ],
        ], $this->builder->build($query));
    }

    public function testQueryBuildsComplexQueries()
    {
        $this->assertEquals(
            [
                'match' => [
                    'message' => [
                        'query' => 'to be or not to be',
                        'operator' => 'and',
                        'zero_terms_query' => 'all',
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('message')
                    ->query('to be or not to be')
                    ->operator('and')
                    ->ZeroTermsQuery('all')
            )
        );

        $this->assertEquals(
            [
                'match' => [
                    'message' => [
                        'query' => 'to be or not to be',
                        'cutoff_frequency' => 0.001,
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('message')
                    ->query('to be or not to be')
                    ->cutoffFrequency(0.001)
            )
        );
    }
}
