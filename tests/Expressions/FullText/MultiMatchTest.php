<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Resolver;
use ElasticsearchQueryBuilder\Expressions\FullText\MultiMatch;
use ElasticsearchQueryBuilder\Contracts\Concerns\FieldsConcern;
use ElasticsearchQueryBuilder\Contracts\Concerns\TieBreakerConcern;
use ElasticsearchQueryBuilder\Contracts\Concerns\TypeConcern;
use ElasticsearchQueryBuilder\Contracts\Queries\MatchQuery;

class MultiMatchTest extends TestCase
{
    protected function query()
    {
        return new MultiMatch;
    }

    public function setUp()
    {
        $this->builder = new Builder(new Resolver);
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query()->key());
        $this->assertEquals('multi_match', $this->query()->key());
    }

    public function testQueryImplementsRequiredContracts()
    {
        $this->assertInstanceOf(MatchQuery::class, $this->query());
        $this->assertInstanceOf(FieldsConcern::class, $this->query());
        $this->assertInstanceOf(TieBreakerConcern::class, $this->query());
        $this->assertInstanceOf(TypeConcern::class, $this->query());
    }

    public function testQueryBuilds()
    {
        $this->assertEquals(
            [
                'multi_match' => [
                    'query' => 'this is a test',
                    'fields' => ['subject', 'message'],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->query('this is a test')
                    ->fields(['subject', 'message'])
            )
        );

        $this->assertEquals(
            [
                'multi_match' => [
                    'query' => 'Will Smith',
                    'fields' => ['title', '*_name'],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->query('Will Smith')
                    ->fields(['title', '*_name'])
            )
        );

        $this->assertEquals(
            [
                'multi_match' => [
                    'query' => 'this is a test',
                    'fields' => ['subject^3', 'message'],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->query('this is a test')
                    ->fields(['subject^3', 'message'])
            )
        );

        $this->assertEquals(
            [
                'multi_match' => [
                    'query' => 'brown fox',
                    'type' => 'best_fields',
                    'fields' => ['subject', 'message'],
                    'tie_breaker' => 0.3,
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->query('brown fox')
                    ->fields(['subject', 'message'])
                    ->type('best_fields')
                    ->tieBreaker(0.3)
            )
        );
    }
}
