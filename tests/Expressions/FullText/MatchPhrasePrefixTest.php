<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Resolver;
use ElasticsearchQueryBuilder\Contracts\Concerns\MaxExpansionsConcern;
use ElasticsearchQueryBuilder\Expressions\FullText\MatchPhrasePrefix;
use ElasticsearchQueryBuilder\Contracts\Queries\MatchPhraseQuery;

class MatchPhrasePrefixTest extends TestCase
{
    protected function query()
    {
        return new MatchPhrasePrefix;
    }

    public function setUp()
    {
        $this->builder = new Builder(new Resolver);
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query()->key());
        $this->assertEquals('match_phrase_prefix', $this->query()->key());
    }

    public function testQueryImplementsRequiredContracts()
    {
        $this->assertInstanceOf(MatchPhraseQuery::class, $this->query());
        $this->assertInstanceOf(MaxExpansionsConcern::class, $this->query());
    }

    public function testQueryBuilds()
    {
        $this->assertEquals(
            [
                'match_phrase_prefix' => [
                    'message' => [
                        'query' => 'quick brown f',
                        'max_expansions' => 10,
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('message')
                    ->query('quick brown f')
                    ->maxExpansions(10)
            )
        );
    }
}
