<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Resolver;
use ElasticsearchQueryBuilder\Expressions\FullText\CommonTerms;

class CommonTermsTest extends TestCase
{
    protected function query()
    {
        return new CommonTerms;
    }

    public function setUp()
    {
        $this->builder = new Builder(new Resolver);
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query()->key());
        $this->assertEquals('common_terms', $this->query()->key());
    }

    public function testQueryImplementsRequiredContracts()
    {
        $this->assertTrue(true);
    }

    public function testQueryBuilds()
    {
        $this->assertEquals(
            [
                'common_terms' => [
                    'body' => [
                        'query' => 'this is bonsai cool',
                        'cutoff_frequency' => 0.001,
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('body')
                    ->query('this is bonsai cool')
                    ->cutoffFrequency(0.001)
            )
        );

        $this->assertEquals(
            [
                'common_terms' => [
                    'body' => [
                        'query' => 'nelly the elephant as a cartoon',
                        'cutoff_frequency' => 0.001,
                        'low_freq_operator' => 'and',
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('body')
                    ->query('nelly the elephant as a cartoon')
                    ->cutoffFrequency(0.001)
                    ->lowFreqOperator('and')
            )
        );

        $this->assertEquals(
            [
                'common_terms' => [
                    'body' => [
                        'query' => 'nelly the elephant as a cartoon',
                        'cutoff_frequency' => 0.001,
                        'minimum_should_match' => [
                            'low_freq' => 2,
                            'high_freq' => 3,
                        ]
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('body')
                    ->query('nelly the elephant as a cartoon')
                    ->cutoffFrequency(0.001)
                    ->MinimumShouldMatch([
                        'low_freq' => 2,
                        'high_freq' => 3,
                    ])
            )
        );
    }
}
