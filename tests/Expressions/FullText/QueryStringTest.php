<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Resolver;
use ElasticsearchQueryBuilder\Expressions\FullText\QueryString;
use ElasticsearchQueryBuilder\Contracts\Concerns\{
    AllowLeadingWildcardConcern,
    AnalyzerConcern,
    AnalyzeWildcardConcern,
    AutoGenerateSynonymsPhraseQueryConcern,
    BoostConcern,
    DefaultFieldConcern,
    DefaultOperatorConcern,
    EnablePositionIncrementsConcern,
    FuzzyMaxExpansionsConcern,
    FuzzyPrefixLengthConcern,
    FuzzyTranspositionsConcern,
    LenientConcern,
    MaxDeterminizedStatesConcern,
    PhraseSlopConcern,
    QuoteAnalyzerConcern,
    QuoteFieldSuffixConcern,
    TimeZoneConcern
};

class QueryStringTest extends TestCase
{
    protected function query()
    {
        return new QueryString;
    }

    public function setUp()
    {
        $this->builder = new Builder(new Resolver);
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query()->key());
        $this->assertEquals('query_string', $this->query()->key());
    }

    public function testQueryImplementsRequiredContracts()
    {
        $this->assertInstanceOf(AllowLeadingWildcardConcern::class, $this->query());
        $this->assertInstanceOf(AnalyzerConcern::class, $this->query());
        $this->assertInstanceOf(AnalyzeWildcardConcern::class, $this->query());
        $this->assertInstanceOf(AutoGenerateSynonymsPhraseQueryConcern::class, $this->query());
        $this->assertInstanceOf(BoostConcern::class, $this->query());
        $this->assertInstanceOf(DefaultFieldConcern::class, $this->query());
        $this->assertInstanceOf(DefaultOperatorConcern::class, $this->query());
        $this->assertInstanceOf(EnablePositionIncrementsConcern::class, $this->query());
        $this->assertInstanceOf(FuzzyMaxExpansionsConcern::class, $this->query());
        $this->assertInstanceOf(FuzzyPrefixLengthConcern::class, $this->query());
        $this->assertInstanceOf(FuzzyTranspositionsConcern::class, $this->query());
        $this->assertInstanceOf(LenientConcern::class, $this->query());
        $this->assertInstanceOf(MaxDeterminizedStatesConcern::class, $this->query());
        $this->assertInstanceOf(PhraseSlopConcern::class, $this->query());
        $this->assertInstanceOf(QuoteAnalyzerConcern::class, $this->query());
        $this->assertInstanceOf(QuoteFieldSuffixConcern::class, $this->query());
        $this->assertInstanceOf(TimeZoneConcern::class, $this->query());
    }

    public function testQueryBuilds()
    {
        $this->assertEquals(
            [
                'query_string' => [
                    'message' => [
                        'query' => 'this is a test',
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('message')
                    ->query('this is a test')
            )
        );
    }
}
