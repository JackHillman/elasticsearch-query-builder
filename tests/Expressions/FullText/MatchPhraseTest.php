<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Resolver;
use ElasticsearchQueryBuilder\Expressions\FullText\MatchPhrase;
use ElasticsearchQueryBuilder\Contracts\Queries\MatchQuery;
use ElasticsearchQueryBuilder\Contracts\Concerns\AnalyzerConcern;
use ElasticsearchQueryBuilder\Contracts\Concerns\ZeroTermsQueryConcern;

class MatchPhraseTest extends TestCase
{
    protected function query()
    {
        return new MatchPhrase;
    }

    public function setUp()
    {
        $this->builder = new Builder(new Resolver);
    }

    public function testQueryHasKey()
    {
        $this->assertNotNull($this->query()->key());
        $this->assertEquals('match_phrase', $this->query()->key());
    }

    public function testQueryImplementsRequiredContracts()
    {
        $this->assertInstanceOf(MatchQuery::class, $this->query());
        $this->assertInstanceOf(AnalyzerConcern::class, $this->query());
        $this->assertInstanceOf(ZeroTermsQueryConcern::class, $this->query());
    }

    public function testQueryBuilds()
    {
        $this->assertEquals(
            [
                'match_phrase' => [
                    'message' => [
                        'query' => 'this is a test',
                        'analyzer' => 'my_analyzer',
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('message')
                    ->query('this is a test')
                    ->analyzer('my_analyzer')
            )
        );

        $this->assertEquals(
            [
                'match_phrase' => [
                    'message' => [
                        'query' => 'this is a test',
                        'analyzer' => 'my_analyzer',
                        'zero_terms_query' => 'all',
                    ],
                ],
            ],
            $this->builder->build(
                $this->query()
                    ->field('message')
                    ->query('this is a test')
                    ->analyzer('my_analyzer')
                    ->zeroTermsQuery('all')
            )
        );
    }
}
