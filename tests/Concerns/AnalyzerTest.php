<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Analyzer;

class AnalyzerTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Analyzer::class);
    }

    public function testAnalyzerExists()
    {
        $this->assertTrue(property_exists($this->mock, 'analyzer'));
    }

    public function testAnalyzerCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'analyzer'));
    }

    public function testAnalyzerIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->analyzer(null));
    }
}
