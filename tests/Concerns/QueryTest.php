<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Query;

class QueryTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Query::class);
    }

    public function testQueryExists()
    {
        $this->assertTrue(property_exists($this->mock, 'query'));
    }

    public function testQueryCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'query'));
    }

    public function testQueryIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->query(null));
    }
}
