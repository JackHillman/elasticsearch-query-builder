<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\FuzzyPrefixLength;

class FuzzyPrefixLengthTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(FuzzyPrefixLength::class);
    }

    public function testFuzzyPrefixLengthExists()
    {
        $this->assertTrue(property_exists($this->mock, 'fuzzy_prefix_length'));
    }

    public function testFuzzyPrefixLengthCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'fuzzyPrefixLength'));
    }

    public function testFuzzyPrefixLengthIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->fuzzyPrefixLength(null));
    }
}
