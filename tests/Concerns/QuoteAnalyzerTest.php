<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\QuoteAnalyzer;

class QuoteAnalyzerTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(QuoteAnalyzer::class);
    }

    public function testQuoteAnalyzerExists()
    {
        $this->assertTrue(property_exists($this->mock, 'quote_analyzer'));
    }

    public function testQuoteAnalyzerCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'quoteAnalyzer'));
    }

    public function testQuoteAnalyzerIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->quoteAnalyzer(null));
    }
}
