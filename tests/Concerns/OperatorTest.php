<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Operator;

class OperatorTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Operator::class);
    }

    public function testOperatorExists()
    {
        $this->assertTrue(property_exists($this->mock, 'operator'));
    }

    public function testOperatorCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'operator'));
        $this->mock->operator();
        $this->assertEquals('and', $this->mock->operator);
        $this->mock->operator('or');
        $this->assertEquals('or', $this->mock->operator);
    }

    public function testOperatorIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->operator());
    }
}
