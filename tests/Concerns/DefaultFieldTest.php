<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\DefaultField;

class DefaultFieldTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(DefaultField::class);
    }

    public function testDefaultFieldExists()
    {
        $this->assertTrue(property_exists($this->mock, 'default_field'));
    }

    public function testDefaultFieldCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'defaultField'));
    }

    public function testDefaultFieldIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->defaultField(null));
    }
}
