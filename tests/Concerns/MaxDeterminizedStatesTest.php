<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\MaxDeterminizedStates;

class MaxDeterminizedStatesTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(MaxDeterminizedStates::class);
    }

    public function testMaxDeterminizedStatesExists()
    {
        $this->assertTrue(property_exists($this->mock, 'max_determinized_states'));
    }

    public function testMaxDeterminizedStatesCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'maxDeterminizedStates'));
    }

    public function testMaxDeterminizedStatesIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->maxDeterminizedStates(null));
    }
}
