<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Type;

class TypeTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Type::class);
    }

    public function testTypeExists()
    {
        $this->assertTrue(property_exists($this->mock, 'type'));
    }

    public function testTypeCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'type'));
    }

    public function testTypeIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->type(null));
    }
}
