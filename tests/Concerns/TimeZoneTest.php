<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\TimeZone;

class TimeZoneTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(TimeZone::class);
    }

    public function testTimeZoneExists()
    {
        $this->assertTrue(property_exists($this->mock, 'time_zone'));
    }

    public function testTimeZoneCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'timeZone'));
    }

    public function testTimeZoneIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->timeZone(null));
    }
}
