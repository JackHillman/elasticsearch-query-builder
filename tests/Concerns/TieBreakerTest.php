<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\TieBreaker;

class TieBreakerTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(TieBreaker::class);
    }

    public function testTieBreakerExists()
    {
        $this->assertTrue(property_exists($this->mock, 'tie_breaker'));
    }

    public function testTieBreakerCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'tieBreaker'));
    }

    public function testTieBreakerIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->tieBreaker(null));
    }
}
