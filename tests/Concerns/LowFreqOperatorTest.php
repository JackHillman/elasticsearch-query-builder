<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\LowFreqOperator;

class LowFreqOperatorTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(LowFreqOperator::class);
    }

    public function testLowFreqOperatorExists()
    {
        $this->assertTrue(property_exists($this->mock, 'low_freq_operator'));
    }

    public function testLowFreqOperatorCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'lowFreqOperator'));
    }

    public function testLowFreqOperatorIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->lowFreqOperator(null));
    }
}
