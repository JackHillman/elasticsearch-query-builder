<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\EnablePositionIncrements;

class EnablePositionIncrementsTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(EnablePositionIncrements::class);
    }

    public function testEnablePositionIncrementsExists()
    {
        $this->assertTrue(property_exists($this->mock, 'enable_position_increments'));
    }

    public function testEnablePositionIncrementsCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'enablePositionIncrements'));
    }

    public function testEnablePositionIncrementsIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->enablePositionIncrements(null));
    }
}
