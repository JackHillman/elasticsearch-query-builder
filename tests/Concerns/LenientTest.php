<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Lenient;

class LenientTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Lenient::class);
    }

    public function testLenientExists()
    {
        $this->assertTrue(property_exists($this->mock, 'lenient'));
    }

    public function testLenientCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'lenient'));
    }

    public function testLenientIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->lenient(null));
    }
}
