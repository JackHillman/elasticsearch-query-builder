<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\AutoGenerateSynonymsPhraseQuery;

class AutoGenerateSynonymsPhraseQueryTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(AutoGenerateSynonymsPhraseQuery::class);
    }

    public function testAutoGenerateSynonymsPhraseQueryExists()
    {
        $this->assertTrue(property_exists($this->mock, 'auto_generate_synonyms_phrase_query'));
    }

    public function testAutoGenerateSynonymsPhraseQueryCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'autoGenerateSynonymsPhraseQuery'));
    }

    public function testAutoGenerateSynonymsPhraseQueryIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->autoGenerateSynonymsPhraseQuery(null));
    }
}
