<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\FuzzyTranspositions;

class FuzzyTranspositionsTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(FuzzyTranspositions::class);
    }

    public function testFuzzyTranspositionsExists()
    {
        $this->assertTrue(property_exists($this->mock, 'fuzzy_transpositions'));
    }

    public function testFuzzyTranspositionsCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'fuzzyTranspositions'));
    }

    public function testFuzzyTranspositionsIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->fuzzyTranspositions(null));
    }
}
