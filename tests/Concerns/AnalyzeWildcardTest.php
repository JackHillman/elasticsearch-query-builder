<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\AnalyzeWildcard;

class AnalyzeWildcardTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(AnalyzeWildcard::class);
    }

    public function testAnalyzeWildcardExists()
    {
        $this->assertTrue(property_exists($this->mock, 'analyze_wildcard'));
    }

    public function testAnalyzeWildcardCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'analyzeWildcard'));
    }

    public function testAnalyzeWildcardIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->analyzeWildcard(null));
    }
}
