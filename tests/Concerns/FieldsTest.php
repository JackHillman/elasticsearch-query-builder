<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Fields;

class FieldsTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Fields::class);
    }

    public function testFieldsExists()
    {
        $this->assertTrue(property_exists($this->mock, 'fields'));
    }

    public function testFieldsCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'fields'));
    }

    public function testFieldsIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->fields(null));
    }
}
