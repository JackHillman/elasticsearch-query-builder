<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\QuoteFieldSuffix;

class QuoteFieldSuffixTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(QuoteFieldSuffix::class);
    }

    public function testQuoteFieldSuffixExists()
    {
        $this->assertTrue(property_exists($this->mock, 'quote_field_suffix'));
    }

    public function testQuoteFieldSuffixCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'quoteFieldSuffix'));
    }

    public function testQuoteFieldSuffixIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->quoteFieldSuffix(null));
    }
}
