<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\FuzzyMaxExpansions;

class FuzzyMaxExpansionsTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(FuzzyMaxExpansions::class);
    }

    public function testFuzzyMaxExpansionsExists()
    {
        $this->assertTrue(property_exists($this->mock, 'fuzzy_max_expansions'));
    }

    public function testFuzzyMaxExpansionsCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'fuzzyMaxExpansions'));
    }

    public function testFuzzyMaxExpansionsIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->fuzzyMaxExpansions(null));
    }
}
