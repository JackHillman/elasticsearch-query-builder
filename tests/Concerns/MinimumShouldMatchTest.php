<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\MinimumShouldMatch;

class MinimumShouldMatchTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(MinimumShouldMatch::class);
    }

    public function testMinimumShouldMatchExists()
    {
        $this->assertTrue(property_exists($this->mock, 'minimum_should_match'));
    }

    public function testMinimumShouldMatchCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'minimumShouldMatch'));
    }

    public function testMinimumShouldMatchIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->minimumShouldMatch(null));
    }
}
