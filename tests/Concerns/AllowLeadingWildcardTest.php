<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\AllowLeadingWildcard;

class AllowLeadingWildcardTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(AllowLeadingWildcard::class);
    }

    public function testAllowLeadingWildcardExists()
    {
        $this->assertTrue(property_exists($this->mock, 'allow_leading_wildcard'));
    }

    public function testAllowLeadingWildcardCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'allowLeadingWildcard'));
    }

    public function testAllowLeadingWildcardIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->allowLeadingWildcard(null));
    }
}
