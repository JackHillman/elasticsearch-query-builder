<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\CutoffFrequency;

class CutoffFrequencyTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(CutoffFrequency::class);
    }

    public function testCutoffFrequencyExists()
    {
        $this->assertTrue(property_exists($this->mock, 'cutoff_frequency'));
    }

    public function testCutoffFrequencyCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'cutoffFrequency'));
    }

    public function testCutoffFrequencyIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->cutoffFrequency(null));
    }
}
