<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\DefaultOperator;

class DefaultOperatorTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(DefaultOperator::class);
    }

    public function testDefaultOperatorExists()
    {
        $this->assertTrue(property_exists($this->mock, 'default_operator'));
    }

    public function testDefaultOperatorCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'defaultOperator'));
    }

    public function testDefaultOperatorIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->defaultOperator(null));
    }
}
