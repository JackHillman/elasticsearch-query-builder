<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\MaxExpansions;

class MaxExpansionsTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(MaxExpansions::class);
    }

    public function testMaxExpansionsExists()
    {
        $this->assertTrue(property_exists($this->mock, 'max_expansions'));
    }

    public function testMaxExpansionsCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'maxExpansions'));
    }

    public function testMaxExpansionsIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->maxExpansions(null));
    }
}
