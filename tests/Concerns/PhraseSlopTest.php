<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\PhraseSlop;

class PhraseSlopTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(PhraseSlop::class);
    }

    public function testPhraseSlopExists()
    {
        $this->assertTrue(property_exists($this->mock, 'phrase_slop'));
    }

    public function testPhraseSlopCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'phraseSlop'));
    }

    public function testPhraseSlopIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->phraseSlop(null));
    }
}
