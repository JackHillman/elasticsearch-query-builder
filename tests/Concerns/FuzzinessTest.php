<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Fuzziness;

class FuzzinessConcern extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Fuzziness::class);
    }

    public function testFuzzinessExists()
    {
        $this->assertTrue(property_exists($this->mock, 'fuzziness'));
    }

    public function testFuzzinessCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'fuzziness'));
        $this->assertNull($this->mock->fuzziness);
        $this->mock->fuzziness();
        $this->assertEquals('AUTO', $this->mock->fuzziness);
        $this->mock->fuzziness(1);
        $this->assertEquals(1, $this->mock->fuzziness);
    }

    public function testFuzzinessHasFuzzyAlias()
    {
        $this->assertTrue(method_exists($this->mock, 'fuzzy'));
        $this->assertNull($this->mock->fuzziness);
        $this->mock->fuzzy();
        $this->assertEquals('AUTO', $this->mock->fuzziness);
    }

    public function testFuzzinessIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->fuzziness());
        $this->assertEquals($this->mock, $this->mock->fuzzy());
    }
}
