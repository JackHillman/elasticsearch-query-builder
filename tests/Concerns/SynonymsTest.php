<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Synonyms;

class SynonymsTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Synonyms::class);
    }

    public function testSynonymsExists()
    {
        $this->assertTrue(property_exists($this->mock, 'synonyms'));
    }

    public function testSynonymsCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'synonyms'));
    }

    public function testSynonymsIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->synonyms(null));
    }
}
