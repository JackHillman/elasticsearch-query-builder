<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\Boost;

class BoostTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(Boost::class);
    }

    public function testBoostExists()
    {
        $this->assertTrue(property_exists($this->mock, 'boost'));
    }

    public function testBoostCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'boost'));
    }

    public function testBoostIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->boost(null));
    }
}
