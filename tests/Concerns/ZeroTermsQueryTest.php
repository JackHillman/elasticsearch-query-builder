<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Concerns\ZeroTermsQuery;

class ZeroTermsQueryTest extends TestCase
{
    public function setUp()
    {
        $this->mock = $this->getMockForTrait(ZeroTermsQuery::class);
    }

    public function testZeroTermsQueryExists()
    {
        $this->assertTrue(property_exists($this->mock, 'zero_terms_query'));
    }

    public function testZeroTermsQueryCanBeSet()
    {
        $this->assertTrue(method_exists($this->mock, 'zeroTermsQuery'));
        $this->mock->zeroTermsQuery();
        $this->assertEquals('none', $this->mock->zero_terms_query);
        $this->mock->zeroTermsQuery('all');
        $this->assertEquals('all', $this->mock->zero_terms_query);
    }

    public function testZeroTermsQueryIsFluent()
    {
        $this->assertEquals($this->mock, $this->mock->zeroTermsQuery());
    }
}
