<?php

use PHPUnit\Framework\TestCase;
use ElasticsearchQueryBuilder\Builder;
use ElasticsearchQueryBuilder\Contracts\ExpressionResolver;

class BuilderTest extends TestCase
{
    public function setUp()
    {
        $resolver = $this->createMock(ExpressionResolver::class);

        $this->builder = new Builder($resolver);
    }

    public function testBuilderHasExpressionResolver()
    {
        $this->assertTrue(method_exists($this->builder, 'resolver'));
        $this->assertInstanceOf(ExpressionResolver::class, $this->builder->resolver());
    }

    public function testBuilderCanBuildQueries()
    {
        $this->assertTrue(method_exists($this->builder, 'build'));
    }
}
