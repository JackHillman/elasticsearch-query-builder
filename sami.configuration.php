<?php

use Sami\Sami;
use Sami\Version\GitVersionCollection;
use Sami\RemoteRepository\GitLabRemoteRepository;

$dir = __DIR__ . '/src';

$versions = GitVersionCollection::create($dir)
    ->add('master');

return new Sami($dir, [
    'versions' => $versions,
    'title' => 'Elasticsearch Query Builder API Docs',
    'build_dir' => __DIR__ . '/public/%version%',
    'cache_dir' => '/tmp/sami/cache/%version%',
    'remote_repository' => new GitLabRemoteRepository(
        'JackHillman/elasticsearch-query-builder',
        dirname($dir)
    ),
]);
