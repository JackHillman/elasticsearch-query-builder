<?php

function snake_case($input)
{
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];

    foreach ($ret as &$match) {
        $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }

    return implode('_', $ret);
}

class Make
{
    const STUBS_DIR = __DIR__ . '/stubs';

    public function __construct($args)
    {
        $start = array_search(basename(__FILE__), $args);
        if ($start !== false) {
            $args = array_slice($args, $start + 1);
        }

        $this->command = array_shift($args);
        $this->args = $args;
    }

    public function run()
    {
        if (count($this->args) === 0 && is_null($this->command)) {
            $this->help();
            die(0);
        }

        if (!$this->isValidCommand()) {
            printf("Command '{$this->command}' not found.\n");
            $this->help();
            die(1);
        }

        $this->parseStubs();
    }

    protected function help()
    {
        printf("\n\e[4mAll available commands\e[m\n\n");

        printf(
            "\e[2m%s %s %s %s\e[0m\n",
            str_pad('Command', 10, ' '),
            str_pad('Title', 10, ' '),
            str_pad('Description', 32, ' '),
            'Arguments'
        );

        foreach ($this->getAvailableCommands() as $command) {
            $info = $this->getInfo($command);

            printf(
                "\e[1m%s\e[0m %s %s \e[2m%s\e[0m\n",
                str_pad($command, 10, ' '),
                str_pad($info['title'], 10, ' '),
                str_pad($info['description'], 32, ' '),
                $info['arguments']
            );
        }

        printf("\n");
    }

    protected function parseStubs()
    {
        $info = $this->getInfo();

        if (count($this->args) < $info['min_args']) {
            printf("Command {$this->command} requires at least {$info['min_args']} arguments.\n");
            die(1);
        }

        foreach ($this->getStubs() as $stub) {
            $handle = fopen($stub, 'r');
            $inHeader = true;
            $headers = [];
            $outputPath = '';
            $file;
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    if (preg_match('/^\<\?php/', $line)) {
                        $inHeader = false;
                        $outputPath = __DIR__ . "/{$headers['PATH']}/{$headers['NAME']}";
                        $file = fopen($outputPath, 'w+');
                    }

                    $line = $this->parseLine($line);

                    if ($inHeader) {
                        $headers = array_merge($headers, $this->parseHeader($line));
                        continue;
                    }

                    fwrite($file, $line);
                }
            }
        }
    }

    protected function isValidCommand()
    {
        return in_array($this->command, $this->getAvailableCommands());
    }

    protected function getAvailableCommands()
    {
        return array_map('basename', glob(static::STUBS_DIR . '/*', GLOB_ONLYDIR));
    }

    protected function getStubs()
    {
        return glob(static::STUBS_DIR . "/{$this->command}/*.php.stub");
    }

    protected function getInfo($command = null)
    {
        $command = $command ?: $this->command;
        $info = [
            'title' => ucfirst($command),
            'description' => '',
            'arguments' => '',
            'min_args' => 0,
        ];

        $file = static::STUBS_DIR . "/{$command}/index.php";
        if (file_exists($file)) {
            $info = array_merge(
                $info,
                (array) include $file
            );
        }

        return $info;
    }

    protected function parseLine($line)
    {
        return preg_replace_callback('/{{\s*(?<expression>.*)\s*}}/U', function ($matches) {
            $expression = preg_replace_callback('/\$(?<i>\d)+/', function ($matches) {
                return '$this->args[' . $matches['i'] . ']';
            }, $matches['expression']);

            $expression = trim($expression);

            return eval("return {$expression};");
        }, $line);
    }

    protected function parseHeader($line)
    {
        $matches = [];
        if (!preg_match('/^(?<key>\w+)\=(?<value>.*)/', $line, $matches)) {
            printf("Failed to parse header line '{$line}'\n");
            die(2);
        }

        extract($matches);

        return [$key => $value];
    }
}

$make = new Make($argv);

$make->run();
