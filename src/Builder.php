<?php

namespace ElasticsearchQueryBuilder;

use ElasticsearchQueryBuilder\Contracts\Builder as BuilderContract;
use ElasticsearchQueryBuilder\Contracts\ExpressionResolver;
use ElasticsearchQueryBuilder\Contracts\Expression;

class Builder implements BuilderContract
{
    /**
     * ElasticsearchQueryBuilder\Contracts\ExpressionResolver
     */
    protected $resolver;

    public function __construct(ExpressionResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * Get the resolver to be used to get each expression class
     *
     * @return \ElasticsearchQueryBuilder\Contracts\ExpressionResolver
     */
    public function resolver()
    {
        return $this->resolver;
    }

    public function build(Expression $query)
    {
        return [
            $query->key() => $query->build(),
        ];
    }
}
