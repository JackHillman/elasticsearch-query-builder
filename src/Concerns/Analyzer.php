<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Analyzer
{
    /**
     * @var mixed|null The analyzer to use for the current query
     */
    public $analyzer;

    /**
     * Set analyzer value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function analyzer($value)
    {
        $this->analyzer = $value;

        return $this;
    }
}
