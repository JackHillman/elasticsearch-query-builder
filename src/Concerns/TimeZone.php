<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait TimeZone
{
    /**
     * @var mixed|null The time_zone to use for the current query
     */
    public $time_zone;

    /**
     * Set time_zone value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function timeZone($value)
    {
        $this->time_zone = $value;

        return $this;
    }
}
