<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Boost
{
    /**
     * @var mixed|null The boost to use for the current query
     */
    public $boost;

    /**
     * Set boost value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function boost($value)
    {
        $this->boost = $value;

        return $this;
    }
}
