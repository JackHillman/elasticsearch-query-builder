<?php

namespace ElasticsearchQueryBuilder\Concerns;

/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#fuzziness
 */
trait Fuzziness
{
    /**
     * @var mixed|null How fuzzy to current query should be
     */
    public $fuzziness;

    /**
     * Set fuzziness value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function fuzziness($value = 'AUTO')
    {
        $this->fuzziness = $value;

        return $this;
    }

    /**
     * Alias for static::fuzziness
     *
     * @see \ElasticsearchQueryBuilder\Concerns\Fuzziness::fuzziness()
     */
    public function fuzzy()
    {
        return call_user_func_array([$this, 'fuzziness'], func_get_args());
    }
}
