<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait FuzzyTranspositions
{
    /**
     * @var mixed|null The fuzzy_transpositions to use for the current query
     */
    public $fuzzy_transpositions;

    /**
     * Set fuzzy_transpositions value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function fuzzyTranspositions($value)
    {
        $this->fuzzy_transpositions = $value;

        return $this;
    }
}
