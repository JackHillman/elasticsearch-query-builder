<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait AllowLeadingWildcard
{
    /**
     * @var mixed|null The allow_leading_wildcard to use for the current query
     */
    public $allow_leading_wildcard;

    /**
     * Set allow_leading_wildcard value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function allowLeadingWildcard($value)
    {
        $this->allow_leading_wildcard = $value;

        return $this;
    }
}
