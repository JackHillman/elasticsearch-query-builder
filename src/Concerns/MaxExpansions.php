<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait MaxExpansions
{
    /**
     * @var mixed|null The max_expansions to use for the current query
     */
    public $max_expansions;

    /**
     * Set max_expansions value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function maxExpansions($value)
    {
        $this->max_expansions = $value;

        return $this;
    }
}
