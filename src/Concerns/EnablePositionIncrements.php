<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait EnablePositionIncrements
{
    /**
     * @var mixed|null The enable_position_increments to use for the current query
     */
    public $enable_position_increments;

    /**
     * Set enable_position_increments value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function enablePositionIncrements($value)
    {
        $this->enable_position_increments = $value;

        return $this;
    }
}
