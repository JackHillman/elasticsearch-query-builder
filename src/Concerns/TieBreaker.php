<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait TieBreaker
{
    /**
     * @var mixed|null The tie_breaker to use for the current query
     */
    public $tie_breaker;

    /**
     * Set tie_breaker value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function tieBreaker($value)
    {
        $this->tie_breaker = $value;

        return $this;
    }
}
