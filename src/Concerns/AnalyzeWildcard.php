<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait AnalyzeWildcard
{
    /**
     * @var mixed|null The analyze_wildcard to use for the current query
     */
    public $analyze_wildcard;

    /**
     * Set analyze_wildcard value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function analyzeWildcard($value)
    {
        $this->analyze_wildcard = $value;

        return $this;
    }
}
