<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait DefaultOperator
{
    /**
     * @var mixed|null The default_operator to use for the current query
     */
    public $default_operator;

    /**
     * Set default_operator value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function defaultOperator($value)
    {
        $this->default_operator = $value;

        return $this;
    }
}
