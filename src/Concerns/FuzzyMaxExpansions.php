<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait FuzzyMaxExpansions
{
    /**
     * @var mixed|null The fuzzy_max_expansions to use for the current query
     */
    public $fuzzy_max_expansions;

    /**
     * Set fuzzy_max_expansions value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function fuzzyMaxExpansions($value)
    {
        $this->fuzzy_max_expansions = $value;

        return $this;
    }
}
