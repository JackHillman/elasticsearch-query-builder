<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait PhraseSlop
{
    /**
     * @var mixed|null The phrase_slop to use for the current query
     */
    public $phrase_slop;

    /**
     * Set phrase_slop value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function phraseSlop($value)
    {
        $this->phrase_slop = $value;

        return $this;
    }
}
