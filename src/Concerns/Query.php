<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Query
{
    /**
     * @var mixed|null The query to use for the current query
     */
    public $query;

    /**
     * Set query value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function query($value)
    {
        $this->query = $value;

        return $this;
    }
}
