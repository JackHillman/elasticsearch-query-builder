<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Fields
{
    /**
     * @var array|null The fields to use for the current query
     */
    public $fields;

    /**
     * Set fields value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function fields($value)
    {
        $this->fields = $value;

        return $this;
    }
}
