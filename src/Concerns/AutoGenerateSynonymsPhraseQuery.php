<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait AutoGenerateSynonymsPhraseQuery
{
    /**
     * @var mixed|null The auto_generate_synonyms_phrase_query to use for the current query
     */
    public $auto_generate_synonyms_phrase_query;

    /**
     * Set auto_generate_synonyms_phrase_query value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function autoGenerateSynonymsPhraseQuery($value)
    {
        $this->auto_generate_synonyms_phrase_query = $value;

        return $this;
    }
}
