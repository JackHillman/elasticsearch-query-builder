<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait QuoteFieldSuffix
{
    /**
     * @var mixed|null The quote_field_suffix to use for the current query
     */
    public $quote_field_suffix;

    /**
     * Set quote_field_suffix value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function quoteFieldSuffix($value)
    {
        $this->quote_field_suffix = $value;

        return $this;
    }
}
