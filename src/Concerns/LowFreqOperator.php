<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait LowFreqOperator
{
    /**
     * @var mixed|null The low_freq_operator to use for the current query
     */
    public $low_freq_operator;

    /**
     * Set low_freq_operator value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function lowFreqOperator($value)
    {
        $this->low_freq_operator = $value;

        return $this;
    }
}
