<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait QuoteAnalyzer
{
    /**
     * @var mixed|null The quote_analyzer to use for the current query
     */
    public $quote_analyzer;

    /**
     * Set quote_analyzer value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function quoteAnalyzer($value)
    {
        $this->quote_analyzer = $value;

        return $this;
    }
}
