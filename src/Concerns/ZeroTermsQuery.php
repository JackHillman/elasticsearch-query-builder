<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait ZeroTermsQuery
{
    /**
     * @var mixed|null The zero_terms_query value to use for the current query
     */
    public $zero_terms_query;

    /**
     * Set zero_terms_query value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function zeroTermsQuery($value = 'none')
    {
        $this->zero_terms_query = $value;

        return $this;
    }
}
