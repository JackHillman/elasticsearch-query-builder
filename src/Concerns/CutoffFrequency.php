<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait CutoffFrequency
{
    /**
     * @var mixed|null The cutoff_frequency to use for the current query
     */
    public $cutoff_frequency;

    /**
     * Set cutoff_frequency value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function cutoffFrequency($value)
    {
        $this->cutoff_frequency = $value;

        return $this;
    }
}
