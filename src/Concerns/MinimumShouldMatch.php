<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait MinimumShouldMatch
{
    /**
     * @var mixed|null The minimum_should_match to use for the current query
     */
    public $minimum_should_match;

    /**
     * Set minimum_should_match value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function minimumShouldMatch($value)
    {
        $this->minimum_should_match = $value;

        return $this;
    }
}
