<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait LowFrequencyOperator
{
    /**
     * @var mixed|null The low_frequency_operator to use for the current query
     */
    public $low_frequency_operator;

    /**
     * Set low_frequency_operator value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function lowFrequencyOperator($value)
    {
        $this->low_frequency_operator = $value;

        return $this;
    }
}
