<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait MaxDeterminizedStates
{
    /**
     * @var mixed|null The max_determinized_states to use for the current query
     */
    public $max_determinized_states;

    /**
     * Set max_determinized_states value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function maxDeterminizedStates($value)
    {
        $this->max_determinized_states = $value;

        return $this;
    }
}
