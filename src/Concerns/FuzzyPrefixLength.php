<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait FuzzyPrefixLength
{
    /**
     * @var mixed|null The fuzzy_prefix_length to use for the current query
     */
    public $fuzzy_prefix_length;

    /**
     * Set fuzzy_prefix_length value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function fuzzyPrefixLength($value)
    {
        $this->fuzzy_prefix_length = $value;

        return $this;
    }
}
