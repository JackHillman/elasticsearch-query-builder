<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Synonyms
{
    /**
     * @var mixed|null The synonyms to use for the current query
     */
    public $synonyms;

    /**
     * Set synonyms value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function synonyms($value)
    {
        $this->synonyms = $value;

        return $this;
    }
}
