<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait DefaultField
{
    /**
     * @var mixed|null The default_field to use for the current query
     */
    public $default_field;

    /**
     * Set default_field value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function defaultField($value)
    {
        $this->default_field = $value;

        return $this;
    }
}
