<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Operator
{
    /**
     * @var mixed|null The operator to use for the current query
     */
    public $operator;

    /**
     * Set operator value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function operator($value = 'and')
    {
        $this->operator = $value;

        return $this;
    }
}
