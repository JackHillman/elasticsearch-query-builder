<?php

namespace ElasticsearchQueryBuilder\Concerns;

trait Lenient
{
    /**
     * @var mixed|null The lenient to use for the current query
     */
    public $lenient;

    /**
     * Set lenient value to use for the current query
     *
     * @param $value mixed
     *
     * @return $this
     */
    public function lenient($value)
    {
        $this->lenient = $value;

        return $this;
    }
}
