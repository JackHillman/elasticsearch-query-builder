<?php

namespace ElasticsearchQueryBuilder\Expressions\Compound;

use ElasticsearchQueryBuilder\Contracts\Expressions\CompoundExpression as CompoundExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class CompoundExpression extends Expression implements CompoundExpressionContract
{

}
