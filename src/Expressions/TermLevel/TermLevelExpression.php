<?php

namespace ElasticsearchQueryBuilder\Expressions\TermLevel;

use ElasticsearchQueryBuilder\Contracts\Expressions\TermLevelExpression as TermLevelExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class TermLevelExpression extends Expression implements TermLevelExpressionContract
{

}
