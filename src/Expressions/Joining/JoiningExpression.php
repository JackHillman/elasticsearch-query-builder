<?php

namespace ElasticsearchQueryBuilder\Expressions\Joining;

use ElasticsearchQueryBuilder\Contracts\Expressions\JoiningExpression as JoiningExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class JoiningExpression extends Expression implements JoiningExpressionContract
{

}
