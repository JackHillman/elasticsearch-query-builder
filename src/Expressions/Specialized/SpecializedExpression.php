<?php

namespace ElasticsearchQueryBuilder\Expressions\Specialized;

use ElasticsearchQueryBuilder\Contracts\Expressions\SpecializedExpression as SpecializedExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class SpecializedExpression extends Expression implements SpecializedExpressionContract
{

}
