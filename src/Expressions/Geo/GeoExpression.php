<?php

namespace ElasticsearchQueryBuilder\Expressions\Geo;

use ElasticsearchQueryBuilder\Contracts\Expressions\GeoExpression as GeoExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class GeoExpression extends Expression implements GeoExpressionContract
{

}
