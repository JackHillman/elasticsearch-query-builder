<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Queries\CommonTermsQuery;
use ElasticsearchQueryBuilder\Concerns\LowFreqOperator;

class CommonTerms extends Match implements CommonTermsQuery,
    \ElasticsearchQueryBuilder\Contracts\Concerns\LowFreqOperatorConcern
{
    use LowFreqOperator;

    protected $key = 'common_terms';

    protected function boot()
    {
        parent::boot();

        $this->concerns = array_merge([
            'low_freq_operator',
        ], $this->concerns);
    }
}
