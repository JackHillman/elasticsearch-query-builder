<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Queries\MatchPhraseQuery;
use ElasticsearchQueryBuilder\Concerns\{
    Analyzer,
    ZeroTermsQuery
};

class MatchPhrase extends Match implements MatchPhraseQuery,
    \ElasticsearchQueryBuilder\Contracts\Concerns\AnalyzerConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\ZeroTermsQueryConcern
{
    use Analyzer, ZeroTermsQuery;

    protected $key = 'match_phrase';

    protected function boot()
    {
        parent::boot();

        $this->concerns = array_merge([
            'analyzer',
            'zero_terms_query',
        ], $this->concerns);
    }
}
