<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Queries\QueryStringQuery;
use ElasticsearchQueryBuilder\Concerns\{
    AllowLeadingWildcard,
    Analyzer,
    AnalyzeWildcard,
    AutoGenerateSynonymsPhraseQuery,
    Boost,
    DefaultField,
    DefaultOperator,
    EnablePositionIncrements,
    FuzzyMaxExpansions,
    FuzzyPrefixLength,
    FuzzyTranspositions,
    Lenient,
    MaxDeterminizedStates,
    PhraseSlop,
    QuoteAnalyzer,
    QuoteFieldSuffix,
    TimeZone
};

class QueryString extends Match implements QueryStringQuery,
    \ElasticsearchQueryBuilder\Contracts\Concerns\AllowLeadingWildcardConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\AnalyzerConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\AnalyzeWildcardConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\AutoGenerateSynonymsPhraseQueryConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\BoostConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\DefaultFieldConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\DefaultOperatorConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\EnablePositionIncrementsConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\FuzzyMaxExpansionsConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\FuzzyPrefixLengthConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\FuzzyTranspositionsConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\LenientConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\MaxDeterminizedStatesConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\PhraseSlopConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\QuoteAnalyzerConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\QuoteFieldSuffixConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\TimeZoneConcern
{
    use AllowLeadingWildcard,
        Analyzer,
        AnalyzeWildcard,
        AutoGenerateSynonymsPhraseQuery,
        Boost,
        DefaultField,
        DefaultOperator,
        EnablePositionIncrements,
        FuzzyMaxExpansions,
        FuzzyPrefixLength,
        FuzzyTranspositions,
        Lenient,
        MaxDeterminizedStates,
        PhraseSlop,
        QuoteAnalyzer,
        QuoteFieldSuffix,
        TimeZone;

    protected $key = 'query_string';

    protected function boot()
    {
        parent::boot();

        $this->concerns = array_merge([
            //
        ], $this->concerns);
    }
}
