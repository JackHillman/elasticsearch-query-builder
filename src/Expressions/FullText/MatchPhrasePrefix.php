<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Queries\MatchPhrasePrefixQuery;
use ElasticsearchQueryBuilder\Concerns\MaxExpansions;

class MatchPhrasePrefix extends MatchPhrase implements MatchPhrasePrefixQuery,
    \ElasticsearchQueryBuilder\Contracts\Concerns\MaxExpansionsConcern
{
    use MaxExpansions;

    protected $key = 'match_phrase_prefix';

    protected function boot()
    {
        parent::boot();

        $this->concerns = array_merge([
            'max_expansions',
        ], $this->concerns);
    }
}
