<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Expressions\FullTextExpression as FullTextExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class FullTextExpression extends Expression implements FullTextExpressionContract
{

}
