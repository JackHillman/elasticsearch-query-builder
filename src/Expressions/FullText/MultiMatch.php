<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Queries\MultiMatchQuery;
use ElasticsearchQueryBuilder\Concerns\{
    Fields,
    TieBreaker,
    Type
};

class MultiMatch extends Match implements MultiMatchQuery,
    \ElasticsearchQueryBuilder\Contracts\Concerns\FieldsConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\TieBreakerConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\TypeConcern
{
    use Fields, TieBreaker, Type;

    protected $key = 'multi_match';

    protected function boot()
    {
        parent::boot();

        $this->concerns = array_merge([
            'fields',
            'tie_breaker',
            'type',
        ], $this->concerns);
    }
}
