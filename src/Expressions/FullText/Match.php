<?php

namespace ElasticsearchQueryBuilder\Expressions\FullText;

use ElasticsearchQueryBuilder\Contracts\Queries\MatchQuery;
use ElasticsearchQueryBuilder\Concerns\{
    Query,
    CutoffFrequency,
    Fuzziness,
    MinimumShouldMatch,
    Operator,
    Synonyms,
    ZeroTermsQuery
};

class Match extends FullTextExpression implements MatchQuery,
    \ElasticsearchQueryBuilder\Contracts\Concerns\QueryConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\CutoffFrequencyConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\FuzzinessConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\MinimumShouldMatchConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\OperatorConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\SynonymsConcern,
    \ElasticsearchQueryBuilder\Contracts\Concerns\ZeroTermsQueryConcern
{
    use Query, CutoffFrequency, Fuzziness, MinimumShouldMatch, Operator, Synonyms, ZeroTermsQuery;

    protected $key = 'match';

    protected function boot()
    {
        parent::boot();

        $this->concerns = array_merge([
            'query',
            'cutoff_frequency',
            'fuzziness',
            'minimum_should_match',
            'operator',
            'synonyms',
            'zero_terms_query',
        ], $this->concerns);
    }
}
