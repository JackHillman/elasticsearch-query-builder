<?php

namespace ElasticsearchQueryBuilder\Expressions;

use ElasticsearchQueryBuilder\Expression;
use ElasticsearchQueryBuilder\Contracts\Queries\MatchAllQuery;

class MatchAll extends Expression implements MatchAllQuery
{
    protected $key = 'match_all';
}
