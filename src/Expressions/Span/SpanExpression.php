<?php

namespace ElasticsearchQueryBuilder\Expressions\Span;

use ElasticsearchQueryBuilder\Contracts\Expressions\SpanExpression as SpanExpressionContract;
use ElasticsearchQueryBuilder\Expression;

abstract class SpanExpression extends Expression implements SpanExpressionContract
{

}
