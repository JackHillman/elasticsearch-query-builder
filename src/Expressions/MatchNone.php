<?php

namespace ElasticsearchQueryBuilder\Expressions;

use ElasticsearchQueryBuilder\Expression;
use ElasticsearchQueryBuilder\Contracts\Queries\MatchNoneQuery;

class MatchNone extends Expression implements MatchNoneQuery
{
    protected $key = 'match_none';
}
