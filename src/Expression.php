<?php

namespace ElasticsearchQueryBuilder;

use ElasticsearchQueryBuilder\Contracts\Expression as ExpressionContract;

abstract class Expression implements ExpressionContract
{
    /**
     * The key to be used when rendering this block
     */
    protected $key;

    /**
     * The field to be used when rendering this block
     */
    protected $field;

    /**
     * Each concerns to automatically build
     */
    protected $concerns = [];

    public function __construct()
    {
        $this->boot();
    }

    protected function boot()
    {
        //
    }

    /**
     * Get the key to be used when rendering this block
     *
     * @return string
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * Set the field to be used when rendering this block
     *
     * @return string
     */
    public function field($value)
    {
        $this->field = $value;

        return $this;
    }

    /**
     * Get the field to be used when rendering this block
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    public function build()
    {
        return empty($this->getField())
            ? $this->concerns()
            : [$this->getField() => $this->concerns()];
    }

    public function beforeBuild(array $query)
    {
        return $query;
    }

    public function afterBuild(array $query)
    {
        return $query;
    }

    protected function concerns()
    {
        $parts = [];

        foreach ($this->concerns as $property) {
            if ($val = $this->$property) {
                $parts[$property] = $val;
            }
        }

        return $parts;
    }
}
