<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface DefaultFieldConcern
{
    public function defaultField($value);
}
