<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface QuoteAnalyzerConcern
{
    public function quoteAnalyzer($value);
}
