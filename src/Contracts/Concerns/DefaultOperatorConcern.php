<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface DefaultOperatorConcern
{
    public function defaultOperator($value);
}
