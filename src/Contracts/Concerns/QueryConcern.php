<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface QueryConcern
{
    public function query($value);
}
