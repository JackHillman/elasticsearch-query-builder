<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface ZeroTermsQueryConcern
{
    public function zeroTermsQuery($value);
}
