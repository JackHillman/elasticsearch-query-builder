<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface TimeZoneConcern
{
    public function timeZone($value);
}
