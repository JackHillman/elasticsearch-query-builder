<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface FieldsConcern
{
    public function fields($value);
}
