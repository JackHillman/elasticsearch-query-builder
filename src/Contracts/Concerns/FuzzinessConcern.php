<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface FuzzinessConcern
{
    public function fuzziness($value);

    public function fuzzy();
}
