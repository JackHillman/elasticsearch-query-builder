<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface AnalyzerConcern
{
    public function analyzer($value);
}
