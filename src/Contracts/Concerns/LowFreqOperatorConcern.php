<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface LowFreqOperatorConcern
{
    public function lowFreqOperator($value);
}
