<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface MaxExpansionsConcern
{
    public function maxExpansions($value);
}
