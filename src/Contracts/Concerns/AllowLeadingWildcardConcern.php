<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface AllowLeadingWildcardConcern
{
    public function allowLeadingWildcard($value);
}
