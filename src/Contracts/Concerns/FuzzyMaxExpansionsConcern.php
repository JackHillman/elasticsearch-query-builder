<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface FuzzyMaxExpansionsConcern
{
    public function fuzzyMaxExpansions($value);
}
