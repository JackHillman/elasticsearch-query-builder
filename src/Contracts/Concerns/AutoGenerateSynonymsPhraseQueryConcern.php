<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface AutoGenerateSynonymsPhraseQueryConcern
{
    public function autoGenerateSynonymsPhraseQuery($value);
}
