<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface CutoffFrequencyConcern
{
    public function cutoffFrequency($value);
}
