<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface BoostConcern
{
    public function boost($value);
}
