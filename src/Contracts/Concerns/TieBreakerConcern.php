<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface TieBreakerConcern
{
    public function tieBreaker($value);
}
