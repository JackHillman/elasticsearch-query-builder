<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface MinimumShouldMatchConcern
{
    public function minimumShouldMatch($value);
}
