<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface AnalyzeWildcardConcern
{
    public function analyzeWildcard($value);
}
