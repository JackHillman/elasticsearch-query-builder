<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface QuoteFieldSuffixConcern
{
    public function quoteFieldSuffix($value);
}
