<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface FuzzyPrefixLengthConcern
{
    public function fuzzyPrefixLength($value);
}
