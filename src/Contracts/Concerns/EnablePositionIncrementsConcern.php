<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface EnablePositionIncrementsConcern
{
    public function enablePositionIncrements($value);
}
