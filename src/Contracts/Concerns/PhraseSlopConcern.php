<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface PhraseSlopConcern
{
    public function phraseSlop($value);
}
