<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface TypeConcern
{
    public function type($value);
}
