<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface LenientConcern
{
    public function lenient($value);
}
