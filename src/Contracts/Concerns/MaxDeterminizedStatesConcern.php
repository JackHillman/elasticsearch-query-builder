<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface MaxDeterminizedStatesConcern
{
    public function maxDeterminizedStates($value);
}
