<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface SynonymsConcern
{
    public function synonyms($value);
}
