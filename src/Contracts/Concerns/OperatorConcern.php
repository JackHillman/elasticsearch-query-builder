<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface OperatorConcern
{
    public function operator($value);
}
