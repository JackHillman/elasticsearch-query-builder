<?php

namespace ElasticsearchQueryBuilder\Contracts\Concerns;

interface FuzzyTranspositionsConcern
{
    public function fuzzyTranspositions($value);
}
