<?php

namespace ElasticsearchQueryBuilder\Contracts\Queries;

/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html
 */
interface MatchQuery
{
    //
}
