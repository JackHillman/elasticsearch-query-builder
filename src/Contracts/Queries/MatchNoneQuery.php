<?php

namespace ElasticsearchQueryBuilder\Contracts\Queries;

/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-all-query.html#query-dsl-match-none-query
 */
interface MatchNoneQuery
{
    //
}
