<?php

namespace ElasticsearchQueryBuilder\Contracts;

interface Builder
{
    public function resolver();

    public function build(Expression $query);
}
