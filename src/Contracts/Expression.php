<?php

namespace ElasticsearchQueryBuilder\Contracts;

interface Expression
{
    public function key();

    public function field($value);

    public function getField();

    public function build();

    public function beforeBuild(array $query);

    public function afterBuild(array $query);
}
