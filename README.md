# Elasticsearch Query Builder

[![pipeline status](https://gitlab.com/JackHillman/elasticsearch-query-builder/badges/master/pipeline.svg)](https://gitlab.com/JackHillman/elasticsearch-query-builder/commits/master)
[![coverage report](https://gitlab.com/JackHillman/elasticsearch-query-builder/badges/master/coverage.svg)](https://gitlab.com/JackHillman/elasticsearch-query-builder/commits/master)

---

Made to support all queries defined in the official [Elasticsearch Query DSL documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html)

---

See the API documentation [here](https://jackhillman.gitlab.io/elasticsearch-query-builder/master/)
